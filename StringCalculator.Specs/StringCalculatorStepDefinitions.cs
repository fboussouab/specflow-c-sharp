﻿using Xunit;

namespace StringCalculator.Specs;

[Binding]
public class StringCalculatorStepDefinitions
{
    private readonly StringCalculator _calculator = new StringCalculator();
    private readonly ScenarioContext _scenarioContext;
    private int _result;
    private string _numbers;

    public StringCalculatorStepDefinitions(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    [Given(@"an empty string")]
    public void GivenAnEmptyString()
    {
        _numbers = "";
    }

    [When(@"the string is calculated")]
    public void WhenTheStringIsCalculated()
    {
        _result = _calculator.calculateString(_numbers);
    }

    [Then(@"the result is (.*)")]
    public void ThenTheResultIs(int res)
    {
        Assert.Equal(res, _result);
    }

    [Given(@"a string ""(.*)""")]
    public void GivenAString(string str)
    {
        _numbers = str;
    }

    [Then(@"the calculation of the string throws a NegativeNumberNotAllowed exception")]
    public void ThenTheCalculationOfTheStringThrowsANegativeNumberNotAllowedException()
    {
        Assert.Throws<NegativeNumberNotAllowed>(() => _calculator.calculateString(_numbers));
    }
}