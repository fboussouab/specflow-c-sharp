﻿namespace StringCalculator;

public class StringCalculator
{
    public int calculateString(String str)
    {
        string sep = ",";
        if (str.Length == 0)
        {
            return 0;
        }

        if (!Char.IsNumber(str[0]))
        {
            sep = str.Substring(0, 1);
            str = str.Substring(1);
        }
        int sum = 0;
        int[] numbers = str.Split(sep).Select(ch => int.Parse(ch)).ToArray();
        foreach (var number in numbers)
        {
            if (number < 0)
            {
                throw new NegativeNumberNotAllowed();
            }

            if (number <= 1000)
            {
                sum += number;
            }
        }

        return sum;
    }
}